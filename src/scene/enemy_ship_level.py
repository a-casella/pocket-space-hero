from src.constants import game_const
from src.scene.level import Level


class EnemyShipLevel(Level):
    """
    This class implements the enemy ship level.

    Attributes
    ----------
    fire_time: float
        Time to wait in order to fire the next laser.
    """
    def __init__(self, num_elements, spawn_time):
        Level.__init__(self, num_elements, spawn_time)
        self.fire_time = game_const.FIRE_TIME

    def next(self):
        self.times_played += 1
        self.num_elements += self.times_played * game_const.ENEMY_NUM_ELEMENTS
        self.spawn_time -= game_const.SPAWN_TIME_DECREASE
        self.fire_time -= game_const.FIRE_TIME_DECREASE
