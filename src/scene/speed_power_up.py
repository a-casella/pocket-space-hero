from src.scene.element import Element
from src.constants import game_const


class SpeedPowerUp(Element):
    """
    This class implements the speed power up which increases the speed of the player.

    Attributes
    ----------
    speed_increase: int
        Speed increase
    """

    def __init__(self, pos_x, pos_y):
        Element.__init__(self, pos_x, pos_y)
        self.speed_increase = game_const.SPEED_INCREASE
        self.curr_sprite = 3
