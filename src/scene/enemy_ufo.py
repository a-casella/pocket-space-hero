from src.scene.element import Element
from src.constants import game_const


class EnemyUFO(Element):
    """
    This class implements the enemy UFO character.

    Attributes
    ----------
    last_laser_time: float
        Time when the enemy last fired a laser.
    direction: int
        Current direction of the enemy UFO.
    strength: int
        Strength of the UFO enemy UFO.
    speed_x: int
        Horizontal speed of the enemy UFO.
    """

    def __init__(self, pos_x, pos_y, direction, strength, speed_x):
        Element.__init__(self, pos_x, pos_y)
        self.last_laser_time = 0
        self.direction = direction
        self.strength = strength
        self.curr_sprite = 1
        self.speed_x = speed_x

    def move(self, direction=None):
        if self.direction == game_const.RIGHT:
            if self.pos_x <= 389:
                self.pos_x += self.speed_x
            else:
                # Switch direction
                self.direction = game_const.LEFT
                self.pos_x -= self.speed_x
        elif self.direction == game_const.LEFT:
            if self.pos_x >= 0:
                self.pos_x -= self.speed_x
            else:
                # Switch direction
                self.direction = game_const.RIGHT
                self.pos_x += self.speed_x
