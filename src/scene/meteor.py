from src.constants import game_const
from src.scene.element import Element


class Meteor(Element):
    """
    This class implements the meteor element.

    Attributes
    ----------
    size: str
        Describes the meteor type.
    strength: int
        How many times a meteor should be hit before it's destroyed.
    direction: str
        Current direction of the meteor.
    is_beyond_limit: bool
        If the meteor is beyond the screen limits.
    """
    def __init__(self, pos_x, size, strength, direction):
        Element.__init__(self, pos_x, 0)
        self.strength = strength
        self.size = size
        self.direction = direction
        self.is_beyond_limit = False

    def move(self, direction=None):
        if self.direction == game_const.RIGHT:
            if self.pos_x <= 450:
                if self.size == game_const.BIG_METEOR:
                    self.pos_x += game_const.X_BIG_METEOR_SPEED
                    self.pos_y += game_const.Y_BIG_METEOR_SPEED
                else:
                    self.pos_x += game_const.X_SMALL_METEOR_SPEED
                    self.pos_y += game_const.Y_SMALL_METEOR_SPEED
            else:
                self.is_beyond_limit = True
        elif self.direction == game_const.LEFT:
            if self.pos_x >= 0:
                if self.size == game_const.BIG_METEOR:
                    self.pos_x -= game_const.X_BIG_METEOR_SPEED
                    self.pos_y += game_const.Y_BIG_METEOR_SPEED
                else:
                    self.pos_x -= game_const.X_SMALL_METEOR_SPEED
                    self.pos_y += game_const.Y_SMALL_METEOR_SPEED
            else:
                self.is_beyond_limit = True
