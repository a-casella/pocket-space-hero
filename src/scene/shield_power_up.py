from src.scene.element import Element
from src.constants import game_const


class ShieldPowerUp(Element):
    """
    This class implements the shield power up which adds a protection shield to the player.

    Attributes
    ----------
    strength: int
        Strength of the shield.
    """

    def __init__(self, pos_x, pos_y):
        Element.__init__(self, pos_x, pos_y)
        self.strength = game_const.SHIELD_STRENGTH
