from src.constants import game_const
from src.scene.level import Level


class EnemyUFOLevel(Level):
    """
    This class implements the enemy UFO level.
    """
    def __init__(self, num_elements, spawn_time):
        Level.__init__(self, num_elements, spawn_time)
        self.speed_x_ufo = game_const.X_ENEMY_UFO_SPEED
        self.fire_time = game_const.FIRE_TIME
        self.strength = game_const.ENEMY_UFO_STRENGTH

    def next(self):
        self.times_played += 1
        self.speed_x_ufo += 1
        self.num_elements = 1
        self.fire_time -= game_const.FIRE_TIME_DECREASE
        self.strength += game_const.ENEMY_UFO_STRENGTH_INCREASE
