from src.scene.element import Element
from src.constants import game_const


class LaserPowerUp(Element):
    """
    This class implements the laser power up which adds laser cannons to the player.

    Attributes
    ----------
    fire_power: int
        Number of laser cannons to add
    """

    def __init__(self, pos_x, pos_y):
        Element.__init__(self, pos_x, pos_y)
        self.fire_power = game_const.FIRE_POWER
        self.curr_sprite = 2
