from src.scene.element import Element
from src.constants import game_const


class LifePowerUp(Element):
    """
    This class implements the life power up which increases lives of the player.

    Attributes
    ----------
    life_increase: int
        Number of lives to add
    """

    def __init__(self, pos_x, pos_y):
        Element.__init__(self, pos_x, pos_y)
        self.life_increase = game_const.LIFE_UP
        self.curr_sprite = 1
