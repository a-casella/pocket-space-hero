from src.scene.element import Element


class HammerPowerUp(Element):
    """
    This class implements the hammer power up which repairs the player.

    """

    def __init__(self, pos_x, pos_y):
        Element.__init__(self, pos_x, pos_y)
        self.curr_sprite = 4

    def repair_player(self, player):
        player.damaged = False
        player.curr_sprite = 0
        return player
