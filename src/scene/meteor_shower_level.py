from src.constants import game_const
from src.scene.level import Level


class MeteorShowerLevel(Level):
    """
    This class implements the meteor shower level.
    """
    def __init__(self, num_elements, spawn_time):
        Level.__init__(self, num_elements, spawn_time)

    def next(self):
        self.num_elements += self.times_played * game_const.METEOR_SHOWER_NUM_ELEMENTS
        self.spawn_time -= 0.1
        self.times_played += 1
