
class Level:
    """
    Base class to implement levels.

    Attributes
    ----------
    num_elements: int
        Number of elements which will be spawned in the current level.
    spawn_time: float
        How much time is required to wait to spawn a new element.
    times_played: int
        A counter to store how many times a level has been played.
    """
    def __init__(self, num_elements, spawn_time):
        self.num_elements = num_elements
        self.spawn_time = spawn_time
        self.times_played = 1

    def next(self):
        pass
