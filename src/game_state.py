from src.scene.enemy_ship_level import EnemyShipLevel
from src.scene.enemy_ufo_level import EnemyUFOLevel
from src.scene.meteor_shower_level import MeteorShowerLevel
from src.scene.player import Player
from src.constants import game_const


class GameState:
    """
    This class provides attributes to represent the state of the game.

    Attributes
    ----------
    enemies: list
        Holds the enemies which are on the screen.
    level: int
        Current level of the game.
    score: int
        The score.
    enemy_lasers: list
        Hold the lases which have been fired by the enemies and still on the screen.
    lasers: list
        Hold the lases which have been fired by the player and still on the screen.
    player: Player
        The player.
    state: str
        The state of the game.
    """

    def __init__(self):
        self.enemies = []
        self.meteors = []
        self.level = 1
        self.is_level_changed = False
        self.score = 0
        self.enemy_lasers = []
        self.lasers = []
        # the player is created using its initial coordinates
        self.player = Player(game_const.INIT_POS_X, game_const.INIT_POS_Y)
        # the game starts with a state which is equal to INIT
        self.state = game_const.INIT
        self.power_ups = []
        self.levels = [EnemyShipLevel(game_const.ENEMY_NUM_ELEMENTS, game_const.ENEMY_SPAWN_TIME),
                       MeteorShowerLevel(game_const.METEOR_SHOWER_NUM_ELEMENTS, game_const.METEOR_SHOWER_SPAWN_TIME),
                       EnemyUFOLevel(game_const.ENEMY_UFO_NUM_ELEMENTS, game_const.ENEMY_UFO_SPAWN_TIME)]

    def next_level(self):
        self.enemies = []
        self.meteors = []
        self.level += 1
        self.enemy_lasers = []
        self.lasers = []

        tmp_head = self.levels[0]
        tmp_head.next()
        tmp_tail = self.levels[2]

        self.levels[0] = self.levels[1]
        self.levels[1] = tmp_tail
        self.levels[2] = tmp_head

        self.is_level_changed = True
