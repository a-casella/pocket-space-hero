import random
import time

from src.scene.enemy_ship import EnemyShip
from src.scene.enemy_ship_level import EnemyShipLevel
from src.scene.enemy_ufo import EnemyUFO
from src.scene.enemy_ufo_level import EnemyUFOLevel
from src.scene.hammer_power_up import HammerPowerUp
from src.scene.laser import Laser
from src.constants import game_const
from src.scene.laser_power_up import LaserPowerUp
from src.scene.life_power_up import LifePowerUp
from src.scene.meteor import Meteor
from src.scene.meteor_shower_level import MeteorShowerLevel
from src.scene.shield_power_up import ShieldPowerUp
from src.scene.speed_power_up import SpeedPowerUp


class Updater:
    """
    This class is responsible for executing the game logic and updating its state.

    Attributes
    ----------
    last_element_spawn: float
        Time when the last enemy was spawned
    time_action: float
        Time when the last action was performed by the player
    """

    def __init__(self):
        self.last_element_spawn = 0
        self.time_action = 0
        # it prevents from creating a new power up at the beginning of the game
        self.last_power_up_time = time.time()
        # the time which must pass to create a new power up
        self.power_up_time = random.randrange(10, 15)

    def update_power_ups(self, game_state):
        """
        Add new power ups on the screen.

        Parameters
        ----------
        game_state: GameState
            The game state.
        Returns
        -------
        game_state: GameState
            The game state.
        """
        curr_time = time.time()

        if (curr_time - self.last_power_up_time >= self.power_up_time and
                len(game_state.power_ups) < game_const.MAX_NUM_POWER_UPS):
            power_up = self.__spawn_power_up(game_state)
            if power_up:
                game_state.power_ups.append(power_up)
                self.last_power_up_time = curr_time
                self.power_up_time = random.randrange(10, 15)
        return game_state

    def __spawn_power_up(self, game_state):
        """
        This method creates a new power up.

        Parameters
        ----------
        game_state: GameState
            The game state.
        Returns
        -------
        power_up: Element
            A new power up.
        """
        power_up = None
        power_up_types = [game_const.SHIELD_POWER_UP,
                          game_const.HAMMER_POWER_UP if game_state.player.damaged else game_const.LIFE_POWER_UP,
                          game_const.LASER_POWER_UP, game_const.SPEED_POWER_UP]
        power_up_choice = random.choice(power_up_types)
        if power_up_choice == game_const.SHIELD_POWER_UP:
            power_up_type = ShieldPowerUp
        elif power_up_choice == game_const.LIFE_POWER_UP:
            power_up_type = LifePowerUp
        elif power_up_choice == game_const.LASER_POWER_UP:
            power_up_type = LaserPowerUp
        elif power_up_choice == game_const.SPEED_POWER_UP:
            power_up_type = SpeedPowerUp
        else:
            power_up_type = HammerPowerUp
        # checks if a power up of random chosen type has been already spawned
        tmp_list = [tmp_power_up for tmp_power_up in game_state.power_ups if isinstance(tmp_power_up, power_up_type)]
        if len(tmp_list) == 0:
            pos_x = random.randrange(45, 415)
            pos_y = random.randrange(155, 225)
            if power_up_choice == game_const.SHIELD_POWER_UP and not (game_state.player.shield and
                                                                      game_state.player.shield.strength > 0):
                power_up = ShieldPowerUp(pos_x, pos_y)
            elif power_up_choice == game_const.LIFE_POWER_UP and game_state.player.lives < 5:
                power_up = LifePowerUp(pos_x, pos_y)
            elif power_up_choice == game_const.LASER_POWER_UP and game_state.player.laser_cannons < 3:
                power_up = LaserPowerUp(pos_x, pos_y)
            elif (power_up_choice == game_const.SPEED_POWER_UP and game_state.player.speed_x < 10 and
                  game_state.player.speed_y < 10):
                power_up = SpeedPowerUp(pos_x, pos_y)
            elif power_up_choice == game_const.HAMMER_POWER_UP and game_state.player.damaged:
                power_up = HammerPowerUp(pos_x, pos_y)
        return power_up

    def update_enemies(self, game_state, enemy_sounds):
        """
        Creates new enemies and updates the existing ones. It also creates lasers fired by enemies.

        Parameters
        ----------
        game_state: GameState
            The game state.
        enemy_sounds: A dictionary of Sound
            The sounds of the enemy.
        Returns
        -------
        game_state: GameState
            The game state.
        """

        for enemy in game_state.enemies:
            enemy.move()
            curr_time = time.time()
            if curr_time - enemy.last_laser_time >= game_state.levels[0].fire_time:
                if isinstance(enemy, EnemyUFO):
                    laser_pos_x = enemy.pos_x
                    laser_pos_y = enemy.pos_y + game_const.ENEMY_UFO_H / 2
                    game_state.enemy_lasers.append(Laser(laser_pos_x, laser_pos_y, game_const.DOWN))
                    laser_pos_x = enemy.pos_x + game_const.ENEMY_UFO_W / 2 - 1
                    laser_pos_y = enemy.pos_y + game_const.ENEMY_UFO_H
                    game_state.enemy_lasers.append(Laser(laser_pos_x, laser_pos_y, game_const.DOWN))
                    laser_pos_x = enemy.pos_x + game_const.ENEMY_UFO_W
                    laser_pos_y = enemy.pos_y + game_const.ENEMY_UFO_H / 2
                    game_state.enemy_lasers.append(Laser(laser_pos_x, laser_pos_y, game_const.DOWN))
                else:
                    laser_pos_x = enemy.pos_x + game_const.ENEMY_SHIP_W / 2 - 1
                    laser_pos_y = enemy.pos_y
                    game_state.enemy_lasers.append(Laser(laser_pos_x, laser_pos_y, game_const.DOWN))
                enemy.last_laser_time = curr_time
                enemy_sounds['laser'].start()
        return game_state

    def update_meteors(self, game_state):
        """
        Creates new meteors and updates the existing ones.

        Parameters
        ----------
        game_state: GameState
            The game state.
        Returns
        -------
        game_state: GameState
            The game state.
        """
        tmp_meteors = []
        for meteor in game_state.meteors:
            meteor.move()
            if not meteor.is_beyond_limit:
                tmp_meteors.append(meteor)
            else:
                game_state.levels[0].num_elements -= 1
        game_state.meteors = tmp_meteors
        return game_state

    def spawn_element(self, game_state):
        """
        Creates new elements based on the current level type.

        Parameters
        ----------
        game_state: GameState
            The game state.
        Returns
        -------
        game_state: GameState
            The game state.
        """
        if isinstance(game_state.levels[0], EnemyShipLevel) or isinstance(game_state.levels[0], EnemyUFOLevel):
            enemy = self.spawn_enemy(game_state.levels[0], game_state)
            if enemy:
                game_state.enemies.append(enemy)
        elif isinstance(game_state.levels[0], MeteorShowerLevel):
            meteor = self.spawn_meteor(game_state.levels[0], game_state)
            if meteor:
                game_state.meteors.append(meteor)
        return game_state

    def spawn_enemy(self, level, game_state):
        """
        Spawns an enemy if conditions are met.

        Parameters
        ----------
        level: Level
            The current level.
        game_state: GameState
            The game state.
        Returns
        -------
        game_state: GameState
            The game state.
        """
        curr_time = time.time()
        if not game_state.enemies or ((curr_time - self.last_element_spawn >= level.spawn_time)
                                      and (len(game_state.enemies) < game_const.MAX_ELEMENTS_ON_SCREEN)
                                      and (len(game_state.enemies) < level.num_elements)):
            self.last_element_spawn = curr_time
            if isinstance(level, EnemyUFOLevel):
                return EnemyUFO(random.randrange(100, 350), 45,
                                random.choice([game_const.RIGHT, game_const.LEFT]),
                                level.strength, level.speed_x_ufo)
            else:
                # create an enemy in a random position on the screen with a random direction (right or left)
                return EnemyShip(random.randrange(45, 415), random.randrange(45, 125),
                                 random.choice([game_const.RIGHT, game_const.LEFT]))

    def spawn_meteor(self, level, game_state):
        """
        Spawns a meteor if conditions are met.

        Parameters
        ----------
        level: Level
            The current level.
        game_state: GameState
            The game state.
        Returns
        -------
        game_state: GameState
            The game state.
        """
        curr_time = time.time()
        if not game_state.meteors or ((curr_time - self.last_element_spawn >= level.spawn_time)
                                      and (len(game_state.meteors) < game_const.MAX_ELEMENTS_ON_SCREEN)
                                      and (len(game_state.meteors) < level.num_elements)):
            size_choice = random.choice([0, 1])
            if size_choice:
                meteor = Meteor(random.randrange(45, 415), game_const.BIG_METEOR, game_const.BIG_METEOR_STRENGTH,
                                random.choice([game_const.RIGHT, game_const.LEFT]))
                meteor.curr_sprite = 0
            else:
                meteor = Meteor(random.randrange(45, 415), game_const.SMALL_METEOR, game_const.SMALL_METEOR_STRENGTH,
                                random.choice([game_const.RIGHT, game_const.LEFT]))
                meteor.curr_sprite = 1
            self.last_element_spawn = curr_time
            return meteor

    def update_player(self, game_state, d_pad, action, player_sounds):
        """
        Updates the player and creates a laser if fired by the player.

        Parameters
        ----------
        game_state: GameState
            The game state.
        d_pad: str
            The direction where to move the player.
        action: str
            The action of the player.
        player_sounds: A dictionary of Sound
            The sounds of the player.
        Returns
        -------
        game_state: GameState
            The game state.
        """
        curr_time = time.time()
        game_state.player.move(d_pad)
        # check if the player has to perform an action and it's passed enough time
        if action != game_const.NONE and (curr_time - self.time_action) >= 0.2:
            if action == game_const.FIRE:
                if game_state.player.laser_cannons == 1:
                    laser_pos_x = game_state.player.pos_x + game_const.PLAYER_W / 2 - 1
                    laser_pos_y = game_state.player.pos_y
                    game_state.lasers.append(Laser(laser_pos_x, laser_pos_y, game_const.UP))
                elif game_state.player.laser_cannons == 2:
                    laser_pos_x = game_state.player.pos_x
                    laser_pos_y = game_state.player.pos_y + game_const.PLAYER_H / 2
                    game_state.lasers.append(Laser(laser_pos_x, laser_pos_y, game_const.UP))
                    laser_pos_x = game_state.player.pos_x + game_const.PLAYER_W - 1
                    laser_pos_y = game_state.player.pos_y + game_const.PLAYER_H / 2
                    game_state.lasers.append(Laser(laser_pos_x, laser_pos_y, game_const.UP))
                elif game_state.player.laser_cannons == 3:
                    laser_pos_x = game_state.player.pos_x
                    laser_pos_y = game_state.player.pos_y + game_const.PLAYER_H / 2
                    game_state.lasers.append(Laser(laser_pos_x, laser_pos_y, game_const.UP))
                    laser_pos_x = game_state.player.pos_x + game_const.PLAYER_W - 1
                    laser_pos_y = game_state.player.pos_y + game_const.PLAYER_H / 2
                    game_state.lasers.append(Laser(laser_pos_x, laser_pos_y, game_const.UP))
                    laser_pos_x = game_state.player.pos_x + game_const.PLAYER_W / 2 - 1
                    laser_pos_y = game_state.player.pos_y
                    game_state.lasers.append(Laser(laser_pos_x, laser_pos_y, game_const.UP))
                self.time_action = curr_time
                player_sounds['laser'].start()
        return game_state

    def update_player_lasers(self, game_state):
        """
        Updates the game state by removing the lasers fired by the player
        which are outside the boundaries of the screen.

        Parameters
        ----------
        game_state: GameState
            The game state.

        Returns
        -------
        game_state: GameState
            The game state.
        """
        tmp_lasers = []
        for laser in game_state.lasers:
            laser.move()
            if laser.pos_y > 0:
                tmp_lasers.append(laser)
        game_state.lasers = tmp_lasers

        return game_state

    def update_enemy_lasers(self, game_state):
        """
        Updates the game state by removing the lasers fired by the enemies
        which are outside the boundaries of the screen.

        Parameters
        ----------
        game_state: GameState
            The game state.

        Returns
        -------
        game_state: GameState
            The game state.
        """
        tmp_lasers = []
        for laser in game_state.enemy_lasers:
            laser.move()
            if laser.pos_y < 480:
                tmp_lasers.append(laser)

        game_state.enemy_lasers = tmp_lasers
        return game_state

    def process_player_collisions_enemies(self, game_state, player_mask, enemy_masks, sound):
        """
        Checks collisions between the player and enemies.

        Parameters
        ----------
        game_state: GameState
            The game state.
        player_mask: CollisionMask
            The collision mask of the player sprite.
        enemy_masks: list of CollisionMask
            List of collision masks of the enemies.
        sound: Sound
            The sound which is played when the player is destroyed.

        Returns
        -------
        game_state: GameState
            The game state.
        """
        for enemy in game_state.enemies:
            tmp_mask = enemy_masks[0] if isinstance(enemy, EnemyShip) else enemy_masks[1]
            if player_mask.collide(tmp_mask, game_state.player.pos_x, game_state.player.pos_y, enemy.pos_x,
                                   enemy.pos_y) > 0:
                game_state.player.respawn()
                game_state.player.lives = game_state.player.lives - 1
                sound.start()
                break
        return game_state

    def process_player_collisions_meteors(self, game_state, player_mask, meteor_masks, sound):
        """
        Checks collisions between the player and a meteor.

        Parameters
        ----------
        game_state: GameState
            The game state.
        player_mask: CollisionMask
            The collision mask of the player sprite.
        meteor_masks: list of CollisionMask
            The collision masks of the meteors.
        sound: Sound
            The sound which is played when the player is destroyed.

        Returns
        -------
        game_state: GameState
            The game state.
        """
        for meteor in game_state.meteors:
            meteor_mask = meteor_masks[0] if meteor.size == game_const.BIG_METEOR else meteor_masks[1]
            if player_mask.collide(meteor_mask, game_state.player.pos_x, game_state.player.pos_y, meteor.pos_x,
                                   meteor.pos_y) > 0:
                game_state.player.respawn()
                game_state.player.lives = game_state.player.lives - 1
                sound.start()
                break
        return game_state

    def process_laser_collisions_enemies(self, game_state, enemy_masks, laser_mask, enemy_sounds):
        """
        Checks collisions between lasers fired by the player and enemies.

        Parameters
        ----------
        game_state: GameState
            The game state.
        enemy_masks: list of CollisionMask
            List of collision masks of the enemies.
        laser_mask: CollisionMask
            The collision mask of the player laser sprite.
        enemy_sounds: dictionary of Sound
            The sounds which are played when the enemy is hit or destroyed.
        Returns
        -------
        game_state: GameState
            The game state.
        """
        tmp_lasers = []
        for laser in game_state.lasers:
            collision = False
            tmp_enemy = None
            for enemy in game_state.enemies:
                tmp_mask = enemy_masks[0] if isinstance(enemy, EnemyShip) else enemy_masks[1]
                if tmp_mask.collide(laser_mask,
                                    enemy.pos_x, enemy.pos_y,
                                    laser.pos_x,
                                    laser.pos_y) > 0:
                    # save temporary the enemy
                    collision = True
                    tmp_enemy = enemy
                    break
            if collision:
                if isinstance(tmp_enemy, EnemyUFO):
                    if tmp_enemy.strength > 0:
                        tmp_enemy.strength -= 1
                        enemy_sounds['enemy_ufo_hit'].start()
                    else:
                        game_state.enemies.remove(tmp_enemy)
                        game_state.levels[0].num_elements -= 1
                        enemy_sounds['enemy_ufo_destroyed'].start()
                else:
                    game_state.levels[0].num_elements -= 1
                    game_state.enemies.remove(tmp_enemy)
                    enemy_sounds['enemy_ship_destroyed'].start()
                game_state.score += game_const.POINTS_INCREMENT
            else:
                tmp_lasers.append(laser)

        game_state.lasers = tmp_lasers
        return game_state

    def process_laser_collisions_meteors(self, game_state, meteor_masks, laser_mask, meteor_sounds):
        """
        Checks collisions between lasers fired by the player and meteors.

        Parameters
        ----------
        game_state: GameState
            The game state.
        meteor_masks: list of CollisionMask
            The collision masks of the meteor element.
        laser_mask: CollisionMask
            The collision mask of the player laser sprite.
        meteor_sounds: dictionary of Sound
            The sounds of the meteor element.
        Returns
        -------
        game_state: GameState
            The game state.
        """
        tmp_lasers = []
        for laser in game_state.lasers:
            collision = False
            tmp_meteor = None
            for meteor in game_state.meteors:
                if meteor.size == game_const.BIG_METEOR:
                    meteor_mask = meteor_masks[0]
                else:
                    meteor_mask = meteor_masks[1]
                if meteor_mask.collide(laser_mask, meteor.pos_x, meteor.pos_y, laser.pos_x, laser.pos_y) > 0:
                    # save temporary hit meteor
                    collision = True
                    tmp_meteor = meteor
                    break
            if collision:
                if tmp_meteor.strength > 0:
                    tmp_meteor.strength -= 1
                    meteor_sounds['hit'].start()
                else:
                    game_state.meteors.remove(tmp_meteor)
                    game_state.levels[0].num_elements -= 1
                    meteor_sounds['destroyed'].start()
                game_state.score += game_const.POINTS_INCREMENT
            else:
                tmp_lasers.append(laser)

        game_state.lasers = tmp_lasers
        return game_state

    def process_laser_collisions_player(self, game_state, player_mask, enemy_laser_mask, shield_sounds, player_sounds):
        """
        Checks collisions between lasers fired by enemies and the player.

        Parameters
        ----------
        game_state: GameState
            The game state.
        player_mask: CollisionMask
            The collision mask of the player sprite.
        enemy_laser_mask: CollisionMask
            The collision mask of the laser enemy sprite.
        shield_sounds: A dictionary of Sound
            The sounds of a shield.
        player_sounds: A dictionary of Sound
            The sounds of the player.
        Returns
        -------
        game_state: GameState
            The game state.
        """
        tmp_lasers = []
        tmp_player_lives = game_state.player.lives
        tmp_player_damaged = game_state.player.damaged
        is_shield_hit = False

        for laser in game_state.enemy_lasers:

            if player_mask.collide(enemy_laser_mask, game_state.player.pos_x, game_state.player.pos_y, laser.pos_x,
                                   laser.pos_y) > 0:
                if game_state.player.shield and game_state.player.shield.strength > 0:
                    game_state.player.shield.strength -= 1
                    is_shield_hit = True
                    if game_state.player.shield.strength == 0:
                        shield_sounds['destroyed'].start()
                    else:
                        shield_sounds['hit'].start()
                elif not game_state.player.damaged:
                    game_state.player.damaged = True
                    game_state.player.curr_sprite = 3
                    player_sounds['hit'].start()
                else:
                    game_state.player.lives = game_state.player.lives - 1
                    game_state.player.respawn()
                    game_state.player.destroyed = True
                    player_sounds['destroyed'].start()
            # no damage has been done (to the player or shield) and the player hasn't been destroyed by the laser
            if (game_state.player.lives == tmp_player_lives and tmp_player_damaged == game_state.player.damaged
                    and not is_shield_hit):
                tmp_lasers.append(laser)

        game_state.enemy_lasers = tmp_lasers
        return game_state

    def process_power_ups_collisions_player(self, game_state, player_mask, power_ups_masks, sound):
        """
        Checks collisions between power ups and the player.

        Parameters
        ----------
        game_state: GameState
            The game state.
        player_mask: CollisionMask
            The collision mask of the player sprite.
        power_ups_masks: CollisionMask
            The collision masks of the power ups
        sound: Sound
            The sound which is played when the power up is activated.
        Returns
        -------
        game_state: GameState
            The game state.
        """
        tmp_power_ups = []

        for power_up in game_state.power_ups:
            if isinstance(power_up, ShieldPowerUp):
                index_mask = 0
            elif isinstance(power_up, LifePowerUp):
                index_mask = 1
            elif isinstance(power_up, LaserPowerUp):
                index_mask = 2
            elif isinstance(power_up, SpeedPowerUp):
                index_mask = 3
            else:
                index_mask = 4
            if player_mask.collide(power_ups_masks[index_mask], game_state.player.pos_x, game_state.player.pos_y,
                                   power_up.pos_x,
                                   power_up.pos_y) > 0:
                if isinstance(power_up, ShieldPowerUp):
                    power_up.pos_x = game_state.player.pos_x - 10
                    power_up.pos_y = game_state.player.pos_y - 10
                    game_state.player.shield = power_up
                elif isinstance(power_up, LifePowerUp):
                    game_state.player.lives += power_up.life_increase
                elif isinstance(power_up, LaserPowerUp):
                    game_state.player.laser_cannons += game_const.FIRE_POWER
                elif isinstance(power_up, SpeedPowerUp):
                    game_state.player.speed_x += power_up.speed_increase
                    game_state.player.speed_y += power_up.speed_increase
                elif isinstance(power_up, HammerPowerUp):
                    game_state.player = power_up.repair_player(game_state.player)
                sound.start()
            else:
                tmp_power_ups.append(power_up)
        game_state.power_ups = tmp_power_ups
        return game_state

    def check_game_state(self, game_state):
        """
        Checks if the game can continue or is over.

        Parameters
        ----------
        game_state: GameState
            The game state.
        Returns
        -------
        str:
            The game state
        """

        if game_state.player.lives <= 0:
            game_state.state = game_const.END
            return game_state
        else:
            game_state.state = game_const.PLAY
            return game_state

    def update_game_level(self, game_state, sound):
        """
        Checks if the game should go to the next level. If so, it performs an update of the game state and attributes.

        Parameters
        ----------
        game_state: GameState
            The game state.
        sound: Sound
            The level up sound.
        Returns
        -------
        game_state: GameState
            The game state.
        """

        if game_state.is_level_changed:
            game_state.is_level_changed = False

        if game_state.levels[0].num_elements == 0:
            game_state.next_level()
            sound.start()
        return game_state
